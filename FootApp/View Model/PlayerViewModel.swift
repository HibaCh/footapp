//
//  PlayerViewModel.swift
//  FootApp
//
//  Created by hiba on 21/9/2022.
//

import Foundation
class PlayerViewModel: ObservableObject {
    
    @Published var  name: String = ""
    @Published var lastName: String = ""
    @Published var  email: String = ""
    @Published var tel: String = ""
    @Published var work: String = ""
    @Published var  status: String = ""
    @Published var  role: String = ""
    @Published var  _id: String = ""
    @Published var playerItems = [PlayerItemViewModel]()
    
    func populateplayers(){
            apiCall().getPlayer{ result in
            switch result {
            case .success(let player):
                DispatchQueue.main.async {
                    self.playerItems = player.data.map(PlayerItemViewModel.init)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    func deletePlayer(playerItem : PlayersModel.playerData){
        apiCall().DeletePlayer(playerId: playerItem._id){ result in
            print(playerItem._id)
            switch result {
                case .success(let response):
                    if let response = response {
                        if response.success {
                            self.populateplayers()
                        }
                    }
                case .failure(let error):
                    print(error,"errorrrr!!!")
                    
            }
        }
    }

    
}


struct PlayerItemViewModel  {
    
    private let player: PlayersModel.playerData
    
    init(player: PlayersModel.playerData) {
        self.player = player
    }
    
    
    var _id: String {
        player._id
    }
    
    var tel: String {
        player.tel
    }
    
    var lastName: String {
        player.lastName
    }
    
    var email: String {
        player.email
    }
    
    var name: String? {
        player.name
    }
    
    var work: String {
        player.work
    }
    var role: String {
        player.role
    }
    var status: String {
        player.status
    }
}
