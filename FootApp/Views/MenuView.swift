//
//  MenuView.swift
//  FootApp
//
//  Created by hiba on 13/9/2022.
//

import SwiftUI

struct MenuView: View {
    var body: some View {
        TabView {
            DashboardView()
                .tabItem{
                    Image(systemName:"house")
                    Text("Dashboard")
                }
            
        
            PlayersView()
                .tabItem{
                    Image(systemName:"person.2.fill")
                    Text("Players")
                }
            
           
            StadiumView()
                .tabItem{
                    Image(systemName:"plus.square.on.square")
                    Text("Stadium")
                }
            
             
            MatchEventView()
                .tabItem{
                    Image(systemName:"rosette")
                    Text("Match Event")
                }
             
            StadiumsView()
                .tabItem{
                    Image(systemName:"rotate.3d")
                    Text("Stadiums")
                }
            
           /* CreateRuleView()
                .tabItem{
                    Image(systemName:"rectangle.and.pencil.and.ellipsis")
                    Text("Create Rule")
                }*/
        }
        .navigationBarHidden(true)
    }
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView()
    }
}

/*       VStack(alignment: .leading) {
 NavigationLink(destination: LoginView(), label: {
     HStack {
         Image("dashboard")
             .resizable()
             .frame(width: 25 , height: 25)
         Text("dashboard")
             .foregroundColor(Color("blue"))
             .font(.headline)
     }
     .padding(.top, 100)
 })
 HStack {
     Image("profile")
         .resizable()
         .frame(width: 25 , height: 25)
     Text("Players")
         .foregroundColor(Color("blue"))
         .font(.headline)
 }
 .padding(.top, 30)
 HStack {
     Image("Stadium")
     .resizable()
     .frame(width: 30 , height: 30)

     Text("Stadium")
         .foregroundColor(Color("blue"))
         .font(.headline)
 }
 .padding(.top, 30)
 HStack {
    Image( "sports")
     .resizable()
     .frame(width: 30 , height: 30)
 
     Text("Match Event")
         .foregroundColor(Color("blue"))
         .font(.headline)
 }
 .padding(.top, 30)
 HStack {
     Image("stadiums")
         .resizable()
         .frame(width: 30 , height: 30)

                            
         //.imageScale(.large)
     Text("Stadiums")
         .foregroundColor(Color("blue"))
         .font(.headline)
 }
 .padding(.top, 30)
 HStack {
     Image("order")
         .resizable()
         .frame(width: 30 , height: 30)
     Text("Create Rule")
         .foregroundColor(Color("blue"))
         .font(.headline)
 }
 .padding(.top, 30)

 Spacer()
}
.padding()
.frame(maxWidth: .infinity, alignment: .leading)
.background(.white)
.edgesIgnoringSafeArea(.all)*/
