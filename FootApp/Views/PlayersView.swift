//
//  PlayersView.swift
//  FootApp
//
//  Created by hiba on 14/9/2022.
//

import SwiftUI

struct PlayersView: View {
    @StateObject private var PlayerListVM = PlayerViewModel()
    @State private var search = ""

    var body: some View {
        ScrollView{ 
            HStack{
                Image(systemName: "magnifyingglass" )
                    .foregroundColor(.gray)
                    .font(.system(size: 24))
                TextField("search", text: $search)
            }
                .padding()
                .overlay(
                RoundedRectangle(cornerRadius:20)
                  .stroke(.gray)
                )
                .padding(.horizontal)
                .frame(width:UIScreen.main.bounds.width - 35)
            
            VStack{
                ForEach (PlayerListVM.playerItems , id: \._id) { player in
                    PlayersBox(PlayerListVM: PlayerListVM, player: PlayersModel.playerData(_id: player._id, name: player.name ?? "", lastName: player.lastName, email: player.email, tel: player.tel, work: player.work, status: player.status, role: player.role) )
                    .padding(.horizontal)
                    .padding(.vertical , 10)
            }
            }.onAppear(perform: {
                PlayerListVM.populateplayers()
            })
        }
        .navigationBarHidden(true)
       
    }
}

struct PlayersView_Previews: PreviewProvider {
    static var previews: some View {
        PlayersView()
    }
}
