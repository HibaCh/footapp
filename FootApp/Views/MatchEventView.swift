//
//  MatchEventView.swift
//  FootApp
//
//  Created by hiba on 14/9/2022.
//

import SwiftUI

struct MatchEventView: View {
    @State var matches = MatchesModel(status: "", data: [] )
    var body: some View {
        VStack{
            ForEach (matches.data , id: \._id) { match in
                Text(match.location)
                .padding(.horizontal)
                .padding(.vertical , 10)
        }
        }
        .onAppear {
            apiCall().getMatches{ matches in
            self.matches = matches
            }

            }
    }
}

struct MatchEventView_Previews: PreviewProvider {
    static var previews: some View {
        MatchEventView()
    }
}
