//
//  LoginView.swift
//  FootApp
//
//  Created by hiba on 12/9/2022.
//

import SwiftUI

struct LoginView: View {
    @State private var sign = false
    var body: some View {
        NavigationView {
            ZStack{
               Rectangle()
                .edgesIgnoringSafeArea(.all)
                .foregroundColor(Color(red: 0.26, green: 0.304, blue: 0.237))
                VStack{
                    ZStack{
                        Image("1")
                        VStack(alignment: .center, spacing: 10){
                        Text("Welcome to ")
                                .foregroundColor(Color(red: 0.916, green: 0.747, blue: 0.453))
                                .font(.system(size:20))
                                .fontWeight(.bold)
                            Text(" FootApp")
                                    .foregroundColor(Color(red: 0.916, green: 0.747, blue: 0.453))
                                    .font(.system(size:35))
                                    .fontWeight(.bold)
                        }
                        .padding(.leading , -160)
                        //.padding(.bottom , 10)
                    }
                    
                    SignInUpForm()

                }
                .frame(maxWidth: .infinity , maxHeight: .infinity, alignment: .top)
                .edgesIgnoringSafeArea(.all)
            }
        }
     
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
