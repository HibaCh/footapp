//
//  StadiumsView.swift
//  FootApp
//
//  Created by hiba on 14/9/2022.
//

import SwiftUI

struct StadiumsView: View {
    @State var stadiums = StadiumsModel(status: "", data: [])
    @State private var search = ""
    var body: some View {
        ScrollView{
            HStack{
                Image(systemName: "magnifyingglass" )
                    .foregroundColor(.gray)
                    .font(.system(size: 24))
                TextField("search", text: $search)
            }
                .padding()
                .overlay(
                RoundedRectangle(cornerRadius:20)
                  .stroke(.gray)
                )
                .padding(.horizontal)
                .frame(width:UIScreen.main.bounds.width - 35)
            
            VStack{
                ForEach (stadiums.data , id: \._id) { stadium in
                    StadiumBox(name: stadium.name, lantitude: Int(stadium.latitude), longitude: Int(stadium.longitude), ResponsibleName: stadium.responsible.name, ResponsiblePhone: stadium.responsible.phone , players: stadium.players)
                    .padding(.horizontal)
                    .padding(.vertical , 10)
            }
            }
            .onAppear {
                apiCall().getStaduims{ stadiums in
                self.stadiums = stadiums
                }

                }
        }
       
        .navigationBarHidden(true)
       
    }
}

struct StadiumsView_Previews: PreviewProvider {
    static var previews: some View {
        StadiumsView()
    }
}
