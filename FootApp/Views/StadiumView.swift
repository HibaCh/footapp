//
//  Stadium.swift
//  FootApp
//
//  Created by hiba on 14/9/2022.
//

import SwiftUI
struct styleTextField: ViewModifier {
    var int : Int
    func body(content: Content) -> some View {
        content
            .padding()
            .overlay(
                RoundedRectangle(cornerRadius:20)
                    .stroke(.gray)
            )
            .padding(.horizontal)
            .padding(.vertical,5)
            .frame(width:UIScreen.main.bounds.width - CGFloat(int))
    }
}
struct StadiumView: View {
    @State private var PlayersNumber = ""
    @State private var StadiumName = ""
    @State private var Latitude = ""
    @State private var responsibleName = ""
    @State private var longitude = ""
    @State private var responsibleNumber = ""
    var body: some View {
        ZStack{
            Rectangle()
                .foregroundColor(.white)
                .frame(width: UIScreen.main.bounds.width - 50 , height:550 , alignment: .center)
                .shadow(color: .primary, radius: 5, x: 0, y: 0)
            VStack{
                Text("Create Stadium:")
                    .font(.system(.title2, design: .rounded))
                    .padding(.bottom)
                TextField("Players Number", text: $PlayersNumber)
                    .modifier(styleTextField(int: 50))
                TextField("Stadium Name", text: $StadiumName)
                    .modifier(styleTextField(int: 50))
                HStack(spacing : -28){
                    TextField("Latitude", text: $Latitude)
                        .modifier(styleTextField(int: 225))
                    TextField("longitude", text: $longitude)
                        .modifier(styleTextField(int: 225))
                }
                TextField("responsible Name", text: $responsibleName)
                    .modifier(styleTextField(int: 50))
                TextField("responsible Number", text: $responsibleNumber)
                    .modifier(styleTextField(int: 50))
                Button(action:{
                    let Parameters = CreateStaduimModel(name: StadiumName, latitude: Double(Latitude) ?? 0, longitude: Double(longitude) ?? 0, players:Int(PlayersNumber) ?? 0, responsible: Responsible(name: responsibleName, phone: responsibleNumber))
                    ApiManager.shareInstance.CreateStaduim(model: Parameters){ (isSuccess) in
                        if isSuccess{
                            PlayersNumber = ""
                            StadiumName = ""
                            Latitude = ""
                            responsibleName = ""
                            longitude = ""
                            responsibleNumber = ""
                            print("success stadium")
                        }else{
                            print("erorrrrrrrrr!!!")
                        }
                    }
                } , label:{
                    Text("Create Stadium")
                        .font(.system(.title3, design: .rounded))
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .frame(width:200, height: 50)
                        .background(Color("blue"))
                        .cornerRadius(50)
                        .padding(.top)
                } )
                
                
            }
        }
        .navigationBarHidden(true)
    }
}

struct StadiumView_Previews: PreviewProvider {
    static var previews: some View {
        StadiumView()
    }
}
