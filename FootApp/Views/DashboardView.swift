//
//  HomeView.swift
//  FootApp
//
//  Created by hiba on 13/9/2022.
//

import SwiftUI

struct DashboardView: View {
    @State var showMenu = false
 
    var body: some View {
    
        
        return NavigationView {
            GeometryReader { geometry in
                ZStack(alignment: .leading) {
                    MainView(showMenu: self.$showMenu)
                        .frame(width: geometry.size.width, height: geometry.size.height)
                        .disabled(self.showMenu ? true : false)
                    if self.showMenu {
                        MenuView()
                            .frame(width: geometry.size.width/2)
                            .transition(.move(edge: .leading))
                    }
                }
            }
    
            
        }
        .navigationBarHidden(true)
    }
}
struct MainView: View {
    @Binding var showMenu: Bool
    var body: some View {
        VStack(alignment: .leading) {
                ZStack{
                    Image("6")
                        .resizable()
                        .cornerRadius(40)
                        .frame(height: 320)
                        .edgesIgnoringSafeArea(.all)
                    
                    VStack{
                        Text("\" We must always set the bar high, otherwise we do not progress \".")
                            .fontWeight(.bold)
                            .multilineTextAlignment(.center)
                        Text("Kylian Mbappe")
                            .font(.system(size: 12))
                            .padding(.leading , -10)
                            .padding(.top , 5)
                    }
                    .frame(width:200)
                    .foregroundColor(.white)
                     .padding(.top , -170)
                     .padding(.leading , -190)
                }
            .frame(maxWidth: .infinity , maxHeight: .infinity, alignment: .top)
          ScrollView{
              VStack(alignment: .leading) {
                   
           Text("Rules")
                    .font(.system(size: 20))
                    .fontWeight(.bold)
                .foregroundColor(Color("blue"))
                .padding(.leading ,15)

                  
                 

                  
                  
                RulesBox()
            }
            .padding(.leading ,25)
              
          }
          .padding(.top , -130)
        }
        .navigationBarHidden(true)
        .onAppear{
          
        }
    }
}
struct DashboardView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardView()
    }
}
