//
//  FootAppApp.swift
//  FootApp
//
//  Created by hiba on 12/9/2022.
//

import SwiftUI

@main
struct FootAppApp: App {
    var body: some Scene {
        WindowGroup {
            LoginView()
        }
    }
}
