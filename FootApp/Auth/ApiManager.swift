//
//  ApiManager.swift
//  FootApp
//
//  Created by hiba on 14/9/2022.
//

import SwiftUI
import Foundation
import Alamofire
enum NetworkError: Error {
    case decodingError
    case badRequest
}
class ApiManager  {
    static let shareInstance = ApiManager()
    func callingRegisterAPI(register:registerModel, completionHandler:@escaping (Bool) -> ()){
        
        let headers: HTTPHeaders = [
            .contentType("application/json")
        ]
        AF.request("https://footballapi.softylines.com/api/v1/register",
                   method: .post ,
                   parameters: register,
                   encoder: JSONParameterEncoder.default ,
                   headers:headers).responseDecodable(of: responseModel.self){response in
            debugPrint(response)
            switch response.result{
            case .success(let data):
                do{
                    UserDefaults.standard.setValue(data.data.token, forKey: "jsonwebtoken")
                    //let json = try JSONSerialization.jsonObject(with: data!,options: [])
                    //print(json)
                    switch (response.response?.statusCode)
                    {
                    case 200:
                        completionHandler(true)
                        
                    default:
                        completionHandler(false)
                        
                    }
                }catch{
                    print(error.localizedDescription)
                    completionHandler(false)
                    
                }
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
 func callingLoginAPI(register:LoginModel, completionHandler:@escaping (Bool) -> ()){
        let headers: HTTPHeaders = [
            .contentType("application/json")
        ]
        
        AF.request("https://footballapi.softylines.com/api/v1/login",
                   method: .post ,
                   parameters: register,
                   encoder: JSONParameterEncoder.default ,
                   headers:headers).responseDecodable(of: responseModel.self){response in
             //debugPrint(response)
            switch response.result{
            case .success(let data):
                do{
                    UserDefaults.standard.setValue(data.data.token, forKey: "jsonwebtoken")
                    if response.response?.statusCode == 200{
                        completionHandler(true)
                    }else{
                        completionHandler(false)
                    }
                }catch{
                    print(error.localizedDescription)
                    completionHandler(false)
                }
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
 
    
    
    func CreateStaduim(model:CreateStaduimModel , completionHandler:@escaping (Bool) -> ()){
        var token = UserDefaults.standard.string(forKey: "jsonwebtoken")
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(token!)",
            "Accept": "application/json"
        ]
        AF.request("https://footballapi.softylines.com/api/v1/stadiums",
                   method: .post ,
                   parameters: model,
                   encoder: JSONParameterEncoder.default ,
                   headers:headers).responseDecodable(of: responseStadiumModel.self){response in
            debugPrint(response)
            switch response.result{
            case .success(let data):
                do{
                    switch (response.response?.statusCode)
                    {
                    case 200:
                        completionHandler(true)
                        
                    default:
                        completionHandler(false)
                        
                    }
                }catch{
                    print(error.localizedDescription)
                    completionHandler(false)
                    
                }
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
        
    }
    

}


class apiCall : ObservableObject {
    

    
    func getrules(completion:@escaping (rulesModel) -> ()) {
        var token = UserDefaults.standard.string(forKey: "jsonwebtoken")
        guard let url = URL(string: "https://footballapi.softylines.com/api/v1/rules") else { return }
       var request = URLRequest(url: url)
        request.allHTTPHeaderFields = [
            "Authorization": "Bearer \(token!)",
            "Accept": "application/json"
        ]
        URLSession.shared.dataTask(with: request) { (data, _, _) in
            let users = try! JSONDecoder().decode(rulesModel.self, from: data!)
            print(users)
            
            DispatchQueue.main.async {
                completion(users)
            }
        }
        .resume()
    }
    
    func getStaduims(completion:@escaping (StadiumsModel) -> ()) {
        var token = UserDefaults.standard.string(forKey: "jsonwebtoken")
        guard let url = URL(string: "https://footballapi.softylines.com/api/v1/stadiums") else { return }
       var request = URLRequest(url: url)
        request.allHTTPHeaderFields = [
            "Authorization": "Bearer \(token!)",
            "Accept": "application/json"
        ]
        URLSession.shared.dataTask(with: request) { (data, _, _) in
            let Stadium = try! JSONDecoder().decode(StadiumsModel.self, from: data!)
          //  print(Stadium)
            
            DispatchQueue.main.async {
                completion(Stadium)
            }
        }
        .resume()
    }
    
    
    
    func getPlayer( completion: @escaping (Result<PlayersModel, NetworkError>) -> Void) {
        var token = UserDefaults.standard.string(forKey: "jsonwebtoken")
        guard let url = URL(string: "https://footballapi.softylines.com/api/v1/users") else { return }
       var request = URLRequest(url: url)
        request.allHTTPHeaderFields = [
            "Authorization": "Bearer \(token!)",
            "Accept": "application/json"
        ]
        URLSession.shared.dataTask(with: request) { data, response, error in

            guard let data = data, error == nil,
                  (response as? HTTPURLResponse)?.statusCode == 200 else {
                      completion(.failure(.badRequest))
                      return
                  }
            
            let todos = try? JSONDecoder().decode(PlayersModel.self, from: data)
            completion(.success(todos!))
        }.resume()
        
    }
    
    
   /* func getPlayer(completion:@escaping (PlayersModel) -> ()) {
        var token = UserDefaults.standard.string(forKey: "jsonwebtoken")
        guard let url = URL(string: "https://footballapi.softylines.com/api/v1/users") else { return }
       var request = URLRequest(url: url)
        request.allHTTPHeaderFields = [
            "Authorization": "Bearer \(token!)",
            "Accept": "application/json"
        ]
        URLSession.shared.dataTask(with: request) { (data, _, _) in
            let player = try! JSONDecoder().decode(PlayersModel.self, from: data!)
          
            
            DispatchQueue.main.async {
                completion(player)
            }
        }
        .resume()
    }*/
    
    
    func getMatches(completion:@escaping (MatchesModel) -> ()) {
        var token = UserDefaults.standard.string(forKey: "jsonwebtoken")
        guard let url = URL(string: "https://footballapi.softylines.com/api/v1/matches") else { return }
       var request = URLRequest(url: url)
        request.allHTTPHeaderFields = [
            "Authorization": "Bearer \(token!)",
            "Accept": "application/json"
        ]
        URLSession.shared.dataTask(with: request) { (data, _, _) in
            let match = try! JSONDecoder().decode(MatchesModel.self, from: data!)
            print(match)
            
            DispatchQueue.main.async {
                completion(match)
            }
        }
        .resume()
    }
    
    
    
/* func DeletePlayer(playerId:String , completion: @escaping (Result<GenericResponse?, NetworkError>) -> Void) {
        var token = UserDefaults.standard.string(forKey: "jsonwebtoken")
        guard let url = URL(string: "https://footballapi.softylines.com/api/v1/users/\(playerId)") else { return }
       var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        request.allHTTPHeaderFields = [
            "Authorization": "Bearer \(token!)",
            "Accept": "application/json"
        ]

        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil,
                  (response as? HTTPURLResponse)?.statusCode == 200 else {
                      completion(.failure(.badRequest))
                      return
                }
            
            let operationResponse = try? JSONDecoder().decode(GenericResponse.self, from: data)
            completion(.success(operationResponse))
            
        }.resume()
    }*/

 func DeletePlayer(playerId:String) {
        var token = UserDefaults.standard.string(forKey: "jsonwebtoken")
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(token!)",
            "Accept": "application/json"
        ]
        AF.request("https://footballapi.softylines.com/api/v1/users/\(playerId)",
                   method: .delete,
                   parameters: nil,
                   headers: headers).validate(statusCode: 200 ..< 299).responseData{ response in
            print(response)
            switch response.result {
                case .success(let data):
                    do {
                        guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                            print("Error: Cannot convert data to JSON object")
                            return
                        }
                        guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                            print("Error: Cannot convert JSON object to Pretty JSON data")
                            return
                        }
                        guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {
                            print("Error: Could print JSON in String")
                            return
                        }

                        print(prettyPrintedJson)
                    } catch {
                        print("Error: Trying to convert JSON data to string")
                        return
                    }
                case .failure(let error):
                    print(error)
            }
        }
    }

}



/*func putMethod(params:PlayersModel ,playerId:String , completionHandler:@escaping (Bool) -> ()) {
    var token = UserDefaults.standard.string(forKey: "jsonwebtoken")
    let headers: HTTPHeaders = [
        "Authorization": "Bearer \(token!)",
        "Accept": "application/json"
    ]
    AF.request( "https://footballapi.softylines.com/api/v1/users/\(playerId)",
                method: .put,
                parameters: params,
                encoder: JSONParameterEncoder.default ,
                headers:headers).responseDecodable(of: responseModel.self){response in
        debugPrint(response)
      switch response.result{
        case .success(let data):
            do{
                switch (response.response?.statusCode)
                {
                case 200:
                    completionHandler(true)
                    
                default:
                    completionHandler(false)
                    
                }
            }catch{
                print(error.localizedDescription)
                completionHandler(false)
                
            }
        case .failure(let err):
            print(err.localizedDescription)
        }
    }
}

*/
