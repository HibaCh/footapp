//
//  editPlayer.swift
//  FootApp
//
//  Created by hiba on 21/9/2022.
//

import SwiftUI

struct editPlayer: View {
    @State  var id = ""
    @Environment(\.presentationMode) var presentationMode
    @State private var tel = ""
    @State private var name = ""
    @State private var lastName = ""
    @State private var email = ""
    @State private var work = ""

    var body: some View {
        VStack{
            TextField("tel", text: $tel)
            TextField("name", text: $name)
            TextField("lastName", text: $lastName)
            TextField("email", text: $email)
            TextField("work", text: $work)
            
        }
        .navigationBarItems(trailing: Button(action: {
         
            self.presentationMode.wrappedValue.dismiss()
               }, label: {
                   Text("Save")
               }))
    }
}

struct editPlayer_Previews: PreviewProvider {
    static var previews: some View {
        editPlayer(id:"")
    }
}
