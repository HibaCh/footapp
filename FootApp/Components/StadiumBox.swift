//
//  StadiumBox.swift
//  FootApp
//
//  Created by hiba on 14/9/2022.
//

import SwiftUI

struct StadiumBox: View {
  @State  var name : String
    @State  var lantitude : Int
    @State  var longitude : Int
    @State  var ResponsibleName : String
    @State  var ResponsiblePhone : String
    @State  var players : Int
    var body: some View {
        ZStack{
            Rectangle()
                 .foregroundColor(.white)
                 .frame(width: UIScreen.main.bounds.width - 70 , height:170 , alignment: .top)
                 .cornerRadius(20)
                 .shadow(color: .primary, radius: 5, x: 0, y: 0)
          HStack(spacing: 30){
                VStack(alignment: .leading , spacing: 5){
                    Text("name: \(name)")
                   HStack{
                       Text("lantitude: \(lantitude)")
                    Text("longitude: \(longitude)")
                       
                   }
                    Text("Responsible name: \(ResponsibleName)")
                    Text("Responsible phone: \(ResponsiblePhone)")
                    Text("players: \(players)")
                }
                .padding(.leading , -10)
               
                HStack(){
                    Button(action: {}, label: {
                        Image(systemName: "pencil.circle")
                            .font(.system(size: 30))
                            .foregroundColor(.green)
                    })
                    Button(action: {}, label: {
                        Image(systemName: "trash.circle")
                            .font(.system(size: 30))
                            .foregroundColor(.red)
                    })

                }
                .padding(.top ,100)

                
               
            }
            
        }
    }
}

struct StadiumBox_Previews: PreviewProvider {
    static var previews: some View {
        StadiumBox(name: "", lantitude: 0, longitude: 0, ResponsibleName: "", ResponsiblePhone: "", players: 0)
    }
}
