//
//  SignInUpBox.swift
//  FootApp
//
//  Created by hiba on 12/9/2022.
//

import SwiftUI

/*struct BorderBottom: ViewModifier {
  var sign : Bool
    func body(content: Content) -> some View {
        VStack{
            content
            if sign {
            Rectangle()
                .frame(width:100 ,height: 1.5, alignment: .bottom)
                .foregroundColor(Color(red: 0.916, green: 0.747, blue: 0.453))
            }
        }
    }
}*/

struct SignInUpBox: View {
    @State private var sign = false
    var body: some View {
        VStack {
           ZStack{
                Rectangle()
               
                   .frame(width: 400,height: sign ? 590 : 300, alignment: .center)
                    .foregroundColor(.white)
                    .cornerRadius(20)
                    .padding()
               VStack{
                   HStack(spacing:100){
                       Button(action: {
                           sign = false
                       }, label: {
                           Text("SIGIN IN")
                               .fontWeight(.bold)
                               .foregroundColor(Color("blue"))
                               .modifier( BorderBottom(sign: sign ? false : true) )
                               .padding(.top , sign ? 20 : -20)
                       })
                       .padding()
                       Button(action: {
                           sign = true
                       }, label: {
                           Text("SIGIN UP")
                               .fontWeight(.bold)
                               .foregroundColor(Color("blue"))
                               .modifier( BorderBottom(sign: sign ? true : false) )
                               .padding(.top , sign ? 20 : -20)
                       })
                       .padding(.vertical)
                   }
                   if sign == false{
                       //SignInForm()
                   }else{
                       SignUpForm()
                           .padding(.bottom)
                   }
                   
               }
            }
            ZStack{
                Circle()
                    .frame(width: 65, height: 65, alignment: .center)
                    .foregroundColor(.white)
                Image(systemName: "arrow.right.circle.fill")
                    .foregroundColor(Color(red: 0.916, green: 0.747, blue: 0.453))
                    .font(.system(size: 50))
            }
            .padding(.top , -50)
        }
    }
}

struct SignInUpBox_Previews: PreviewProvider {
    static var previews: some View {
        SignInUpBox()
            .preferredColorScheme(.dark)
    }
}
