//
//  RulesBox.swift
//  FootApp
//
//  Created by hiba on 13/9/2022.
//

import SwiftUI
struct textRule: ViewModifier {
    func body(content: Content) -> some View {
        HStack{
            Image(systemName: "pin.fill" )
                .foregroundColor(.white)
            content
                .foregroundColor(.white)
        }
    }
}
struct RulesBox: View {
    @State var users = rulesModel(status: "", data: [])
    var body: some View {
        ZStack{
            Rectangle()
                .foregroundColor( Color("blue"))
                .frame(width: UIScreen.main.bounds.width - 50, height: 300, alignment: .center)
                .cornerRadius(30)
            VStack(alignment: .leading){
                Button(action: {}, label: {
                    Image(systemName:"rectangle.and.pencil.and.ellipsis")
                        .foregroundColor(.white)
                        .font(.system(size: 30))
                        .padding(.leading , 280)
                        .padding(.bottom , 20)
                })
                VStack{
                ForEach(users.data , id: \._id) { user in
                    Text(user.content)
                        .modifier(textRule())
                  }
                }
                .onAppear {
                apiCall().getrules { users in
                self.users = users
                }

                }
                .frame(width: UIScreen.main.bounds.width - 100)
                
                
            }
        }
        
    }
}

struct RulesBox_Previews: PreviewProvider {
    static var previews: some View {
        RulesBox()
    }
}



