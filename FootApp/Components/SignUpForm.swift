//
//  SignUpForm.swift
//  FootApp
//
//  Created by hiba on 12/9/2022.
//

import SwiftUI





struct SignUpForm: View {
    @State private var FirstNameText = ""
    @State private var LastNameText = ""
    @State private var EmailText = ""
    @State private var PhoneText = ""
    @State private var PlayerWorkText = ""
    @State private var PasswordText = ""
    @State private var PasswordConfirmText = ""
    var body: some View {
        VStack(spacing:10){
            TextField(" First name", text: $FirstNameText)
                .modifier(StyleOfAmount(img: "person"))
            TextField("Last name", text: $LastNameText)
                .modifier(StyleOfAmount(img: "person"))
            TextField("Email address", text: $EmailText)
                .modifier(StyleOfAmount(img: "envelope"))
            TextField("Phone Number", text: $PhoneText)
                .modifier(StyleOfAmount(img: "phone.fill"))
            TextField("Player Work", text: $PlayerWorkText)
                .modifier(StyleOfAmount(img: "briefcase"))
            SecureField("Password", text: $PasswordText)
                .modifier(StyleOfAmount(img: "lock.fill"))
            SecureField("Password Confirm", text: $PasswordConfirmText)
                .modifier(StyleOfAmount(img: "lock.fill"))
               
        }
    }
}

struct SignUpForm_Previews: PreviewProvider {
    static var previews: some View {
        SignUpForm()
    }
}
