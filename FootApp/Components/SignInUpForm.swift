//
//  SignInForm.swift
//  FootApp
//
//  Created by hiba on 12/9/2022.
//

import SwiftUI
struct BorderBottom: ViewModifier {
  var sign : Bool
    func body(content: Content) -> some View {
        VStack{
            content
            if sign {
            Rectangle()
                .frame(width:100 ,height: 1.5, alignment: .bottom)
                .foregroundColor(Color(red: 0.916, green: 0.747, blue: 0.453))
            }
        }
    }
}
struct StyleOfAmount: ViewModifier {
    var img : String
    func body(content: Content) -> some View {
        HStack{
            Image(systemName: img )
                .foregroundColor(.gray)
            content
        }
            .padding()
            .overlay(
            RoundedRectangle(cornerRadius:50)
              .stroke(.gray)
            )
            .padding(.horizontal)
            .frame(width:UIScreen.main.bounds.width - 50)
    }
}
struct SignInUpForm: View {
    @State private var showLinkTarget = false
    @State private var sign = false
    @State private var FirstNameText = ""
    @State private var LastNameText = ""
    @State private var EmailText = ""
    @State private var PhoneText = ""
    @State private var PlayerWorkText = ""
    @State private var PasswordText = ""
    @State private var PasswordConfirmText = ""

    var body: some View {
        
        VStack {
            
            NavigationLink(
                destination:HomeView()
                , isActive: self.$showLinkTarget ){
                EmptyView()
            }

           ZStack{
                Rectangle()
               
                   .frame(width: 400,height: sign ? 590 : 300, alignment: .center)
                    .foregroundColor(.white)
                    .cornerRadius(20)
                    .padding()
               VStack{
                   HStack(spacing:100){
                       Button(action: {
                           sign = false
                       }, label: {
                           Text("SIGIN IN")
                               .fontWeight(.bold)
                               .foregroundColor(Color("blue"))
                               .modifier( BorderBottom(sign: sign ? false : true) )
                               .padding(.top , sign ? 20 : -20)
                       })
                       .padding()
                       Button(action: {
                           sign = true
                       }, label: {
                           Text("SIGIN UP")
                               .fontWeight(.bold)
                               .foregroundColor(Color("blue"))
                               .modifier( BorderBottom(sign: sign ? true : false) )
                               .padding(.top , sign ? 20 : -20)
                       })
                       .padding(.vertical)
                   }
                   if sign == false{
                       VStack(spacing:10){
                       
                           TextField("Email address", text: $EmailText)
                               .modifier(StyleOfAmount(img: "envelope"))
                           SecureField("Password", text: $PasswordText)
                               .modifier(StyleOfAmount(img: "lock.fill"))
                   Text("Forget Password")
                               .foregroundColor(Color("blue"))
                               //.padding(.top , 30)
                       }
                   }else{
                       VStack(spacing:10){
                           TextField(" First name", text: $FirstNameText)
                               .modifier(StyleOfAmount(img: "person"))
                           TextField("Last name", text: $LastNameText)
                               .modifier(StyleOfAmount(img: "person"))
                           TextField("Email address", text: $EmailText)
                               .modifier(StyleOfAmount(img: "envelope"))
                           TextField("Phone Number", text: $PhoneText)
                               .modifier(StyleOfAmount(img: "phone.fill"))
                           TextField("Player Work", text: $PlayerWorkText)
                               .modifier(StyleOfAmount(img: "briefcase"))
                           SecureField("Password", text: $PasswordText)
                               .modifier(StyleOfAmount(img: "lock.fill"))
                           SecureField("Password Confirm", text: $PasswordConfirmText)
                               .modifier(StyleOfAmount(img: "lock.fill"))
                              
                       }
                           .padding(.bottom)
                   }
                   
               }
            }
            
            
        
            
            
            Button(action: {
                
            //  ApiManager.shareInstance.callingGetAPI()
                
                let SignUpParameters = registerModel(name: FirstNameText, lastName: LastNameText, email: EmailText, password: PasswordText, passwordConfirm: PasswordConfirmText, tel: PhoneText, work: PlayerWorkText)
                let SignInParameters = LoginModel(email: EmailText, password: PasswordText)
                if sign == false{
                ApiManager.shareInstance.callingLoginAPI(register: SignInParameters)
                { (isSuccess) in
                    if isSuccess{
                        print("login success")
                        showLinkTarget = true
                        print(showLinkTarget)
                    }
                    else{
                        print("login error")
                    }
                }
                }else{
                    ApiManager.shareInstance.callingRegisterAPI(register: SignUpParameters)
                    { (isSuccess) in
                        if isSuccess{
                            print("register success")
                            showLinkTarget = true
                            print(showLinkTarget)
                        }
                        else{
                          print("register error")
                        }
                    }
                }
                   
              
                
           

            }, label: {
                ZStack{
                    Circle()
                        .frame(width: 65, height: 65, alignment: .center)
                        .foregroundColor(.white)
                    Image(systemName: "arrow.right.circle.fill")
                        .foregroundColor(Color(red: 0.916, green: 0.747, blue: 0.453))
                        .font(.system(size: 50))
                }
            })
            .disabled(EmailText.isEmpty || PasswordText.isEmpty)
            .padding(.top , -50)
        }
            .padding(.top , -120)
            .padding(.bottom)
        
        
   
    }
    

}

struct SignInUpForm_Previews: PreviewProvider {
    static var previews: some View {
        SignInUpForm()
         
    }
}
