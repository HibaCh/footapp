//
//  PlayersBox.swift
//  FootApp
//
//  Created by hiba on 14/9/2022.
//

import SwiftUI

struct PlayersBox: View {
  
    @ObservedObject  var PlayerListVM: PlayerViewModel

    var player : PlayersModel.playerData
    
    var body: some View {
        ZStack{
            Rectangle()
                 .foregroundColor(.white)
                 .frame(width: UIScreen.main.bounds.width - 70 , height:170 , alignment: .top)
                 .cornerRadius(20)
                 .shadow(color: .primary, radius: 5, x: 0, y: 0)
          HStack(spacing: 30){
                VStack(alignment: .leading , spacing: 10){
                    Text("name: \(player.name) \(player.lastName)")
                    Text("rule:  \(player.role)")
                    Text("email:  \(player.email)")
                    Text("status:  \(player.status)")
                }
                .padding(.leading , -10)
               
                HStack(){
                
                    NavigationLink(destination: editPlayer(id:player._id) , label:{
                                Image(systemName: "pencil.circle")
                                    .font(.system(size: 30))
                                    .foregroundColor(.green)
                            
                        } )
                    
                    Button(action: {
                       // PlayerListVM.deletePlayer(playerItem: PlayerItemViewModel(player: player) )
                    }, label: {
                        Image(systemName: "trash.circle")
                            .font(.system(size: 30))
                            .foregroundColor(.red)
                    })

                }
                .padding(.top ,100)

                
               
            }
            
        }
    }
}

struct PlayersBox_Previews: PreviewProvider {
    static var previews: some View {
        PlayersBox(PlayerListVM: PlayerViewModel(), player: PlayersModel.playerData(_id: "", name: "", lastName: "", email: "", tel: "", work: "", status: "", role: ""))
    }
}
