//
//  registerModel.swift
//  FootApp
//
//  Created by hiba on 14/9/2022.
//

import Foundation
struct registerModel:Codable{
 let name: String
 let lastName: String
 let email: String
 let password: String
 let passwordConfirm: String
 let tel: String
 let work: String
}
