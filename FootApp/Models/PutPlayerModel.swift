//
//  PutPlayerModel.swift
//  FootApp
//
//  Created by hiba on 21/9/2022.
//

import Foundation
struct PutPlayerModel: Codable{
    let   _id: String
    let  name: String
    let  lastName: String
    let  email: String
    let  tel: String
    let  work: String
    let  status: String
    let  role: String
}
