//
//  PlayersModel.swift
//  FootApp
//
//  Created by hiba on 19/9/2022.
//

import Foundation


    struct PlayersModel : Codable {
        let status: String
        let data:  Array<playerData>
        struct playerData: Codable{
            let   _id: String
            let  name: String
            let  lastName: String
            let  email: String
            let  tel: String
            let  work: String
            let  status: String
            let  role: String
        }
        
    }
  

   
    


