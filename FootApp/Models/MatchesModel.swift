//
//  MatchesModel.swift
//  FootApp
//
//  Created by hiba on 21/9/2022.
//

import Foundation


struct MatchesModel : Codable {
    let status: String
    let data: Array<matcheData>
    
    
    struct matcheData : Codable {
        let   _id:String
        let   location: String
        let   stadium: DataSStadiums
      let   participants:Array<DataPplayer>
        let startsAt: String
        let  endsAt: String
        
        
        struct  DataSStadiums : Codable {
            let  responsible :resppData
            let _id: String
            let  name: String
            let  latitude: Double
            let  longitude: Double
            let  players: Int
            
            struct resppData: Codable{
                let   name: String
                let   phone: String
            }
        }
        
        struct DataPplayer: Codable{
            let   _id: String
            let  name: String
            let  lastName: String
            let  email: String
            let  tel: String
            let  work: String
            let  status: String
            let  role: String
        }
        
    }
    
}







