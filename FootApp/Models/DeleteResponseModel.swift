//
//  DeleteResponseModel.swift
//  FootApp
//
//  Created by hiba on 20/9/2022.
//

import Foundation

struct DeleteResponseModel : Codable{
  let  status: String
   let message: String
}
