//
//  StadiumsModel.swift
//  FootApp
//
//  Created by hiba on 19/9/2022.
//

import Foundation

struct StadiumsModel : Codable {
    let status: String
    let data:  Array<StadiumsData>
}

struct  StadiumsData : Codable {
    let  responsible :responsibleData
    let _id: String
    let  name: String
    let  latitude: Double
    let  longitude: Double
    let  players: Int
}

struct responsibleData: Codable{
    let   name: String
    let   phone: String
}
