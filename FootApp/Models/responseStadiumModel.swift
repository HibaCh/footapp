//
//  responseStadiumModel.swift
//  FootApp
//
//  Created by hiba on 20/9/2022.
//

import Foundation
struct responseStadiumModel : Codable{
  let  status: String
    let message:String
    let  data: DataStaduimModel
}
struct DataStaduimModel : Codable{
    let name: String
    let latitude: Double
    let longitude: Double
    let players: Int
    let  responsible:ResponsibleData
    let _id: String
    let   createdAt: String
    let   updatedAt:String
    let   __v: Int
}
struct ResponsibleData:Codable{
    let   name: String
    let  phone: String
}
