//
//  GenericResponse.swift
//  FootApp
//
//  Created by hiba on 21/9/2022.
//

import Foundation

struct GenericResponse: Codable {
    let success: Bool
    let message: String
}
