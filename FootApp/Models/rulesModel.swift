//
//  rulesModel.swift
//  FootApp
//
//  Created by hiba on 16/9/2022.
//

import Foundation

struct rulesModel : Codable {
 let   status: String
   let data:  Array<DataRules>
}
struct  DataRules  : Codable   {
    let   _id: String
     let  content: String
     let  createdAt: String
      let updatedAt: String
      let __v: Int
   }




