//
//  CreateStaduimModel.swift
//  FootApp
//
//  Created by hiba on 19/9/2022.
//

import Foundation
struct CreateStaduimModel : Codable{
    let name: String
    let latitude: Double
    let longitude: Double
    let players: Int
    let  responsible:Responsible
}
struct Responsible:Codable{
    let   name: String
    let  phone: String
}
